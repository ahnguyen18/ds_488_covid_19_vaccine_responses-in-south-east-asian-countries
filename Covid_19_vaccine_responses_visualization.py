import psycopg2
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

connectionString = "dbname='ahnguyen18_db' user='ahnguyen18'"
connection = psycopg2.connect(connectionString)

cursor = connection.cursor()

sqlStatement1 = "SELECT Country_name, Doses_per_100, Doses_total, Percentage_vaccinated, Percentage_fully_vaccinated FROM Covid_19_Vaccine;"

cursor.execute(sqlStatement1)


records = cursor.fetchall()

Country_name = []
Doses_per_100 = []
Doses_total = []
Percentage_vaccinated = []
Percentage_fully_vaccinated = []

for row in records[:200]:
    Country_name.append(row[0].encode('utf-8'))
    Doses_per_100.append(row[1])
    Doses_total.append(row[2])
    Percentage_vaccinated.append(row[3])
    Percentage_fully_vaccinated.append(row[4])

fig = plt.figure(figsize = (17, 8))    
plt.bar(Country_name, Doses_per_100, color ='red', width = 0.5)
plt.xlabel('Country_name')
plt.ylabel('Doses_per_100')
plt.savefig("Doses_per_100 vs Countries.png")

fig = plt.figure(figsize = (17, 8)) 
plt.bar(Country_name, Doses_total, color ='green', width = 0.5)
plt.xlabel('Countries')
plt.ylabel('Doses_total')
plt.savefig("Countries vs Doses_total.png")

fig = plt.figure(figsize = (50, 8)) 
plt.bar(Country_name, Percentage_vaccinated, color ='blue', width = 0.5)
plt.xlabel('Country_name')
plt.ylabel('Percentage_vaccinated')
plt.savefig("Countries vs Percentage_vaccinated.png")

fig = plt.figure(figsize = (50, 8)) 
plt.bar(Country_name, Percentage_fully_vaccinated, color ='yellow', width = 0.5)
plt.xlabel('Country_name')
plt.ylabel('Percentage_fully_vaccinated')
plt.savefig("Countries vs Percentage_fully_vaccinated.png")